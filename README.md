# Probability Cheat Sheet

*Tips and tricks about general probability (undergraduate level)*

[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)


## Compile
```shell
$ pdflatex book/Book.tex
```
*or open a `.tex` editor and compile form the GUI.*


## Usage
```shell
$ cat book/Book.pdf | read-all | contribute
```

## Thanks
Thanks to anyone who ever loved this project. Contributions are welcomed!


## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
